package com.study.shop.qo;

import lombok.Data;

@Data
public class UserEntityQo {

    private String userName;

    private Integer roleId;

    private Long current;

    private Long size;

}
