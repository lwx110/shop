package com.study.shop.qo;

import lombok.Data;

@Data
public class ProductEntityQo {

    private String productName;

    private Integer typeId;

    private Long current;

    private Long size;

}
