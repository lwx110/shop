package com.study.shop.qo;

import lombok.Data;


@Data
public class OrderDetailEntityQo {


    private int productId;

    private int num;

    private double price;

}
