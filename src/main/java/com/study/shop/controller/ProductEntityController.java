package com.study.shop.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.study.shop.base.ResponseData;
import com.study.shop.entity.ProductEntity;
import com.study.shop.qo.ProductEntityQo;
import com.study.shop.service.ProductEntityService;
import com.study.shop.vo.ProductEntityVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/product")
@Slf4j
public class ProductEntityController {

    @Resource
    private ProductEntityService productEntityService;

    /**
     * 查询产品列表
     * */
    @GetMapping("/list")
    public ResponseData<List<ProductEntityVo>> list(ProductEntityQo qo){
        return new ResponseData<List<ProductEntityVo>>(productEntityService.list(qo));
    }

    /**
     * 分页查询产品
     * */
    @GetMapping("/pageList")
    public ResponseData<Page<ProductEntityVo>> pageList(ProductEntityQo qo){
        return new ResponseData<Page<ProductEntityVo>>(productEntityService.pageList(qo));
    }

    /**
     * 查询产品
     * */
    @GetMapping("/detail/{id}")
    public ResponseData<ProductEntityVo> pageList(@PathVariable("id") int id){
        return new ResponseData<ProductEntityVo>(productEntityService.detail(id));
    }

    /**
     * 添加产品
     * */
    @PostMapping("/add")
    public ResponseData<Boolean> add(@RequestBody ProductEntity productEntity){
        return new ResponseData<Boolean>(productEntityService.save(productEntity));
    }

    /**
     * 修改产品
     * */
    @PutMapping("/update")
    public ResponseData<Boolean> update(@RequestBody ProductEntity productEntity){
        return new ResponseData<Boolean>(productEntityService.updateById(productEntity));
    }

    /**
     * 删除产品
     * */
    @DeleteMapping("/delete/{id}")
    public ResponseData<Boolean> delete(@PathVariable("id") int id){
        return new ResponseData<Boolean>(productEntityService.removeById(id));
    }


}
