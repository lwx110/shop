package com.study.shop.controller;

import com.study.shop.base.ResponseData;
import com.study.shop.entity.CartEntity;
import com.study.shop.service.CartEntityService;
import com.study.shop.vo.CartEntityVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/cart")
@Slf4j
public class CartEntityController {

    @Resource
    private CartEntityService cartEntityService;

    /**
     * 查询购物车列表
     * */
    @GetMapping("/list")
    public ResponseData<List<CartEntityVo>> list(){
        return new ResponseData<List<CartEntityVo>>(cartEntityService.listAll());
    }

    /**
     * 查询某个用户购物车
     * */
    @GetMapping("/detail/{userId}")
    public ResponseData<CartEntityVo> pageList(@PathVariable("userId") int userId){
        return new ResponseData<CartEntityVo>(cartEntityService.detail(userId));
    }

    /**
     * 添加购物车
     * */
    @PostMapping("/add")
    public ResponseData<Boolean> add(@RequestBody CartEntity cartEntity){
        return new ResponseData<Boolean>(cartEntityService.save(cartEntity));
    }

    /**
     * 修改购物车
     * */
    @PutMapping("/update")
    public ResponseData<Boolean> update(@RequestBody CartEntity cartEntity){
        return new ResponseData<Boolean>(cartEntityService.updateById(cartEntity));
    }

    /**
     * 删除购物车
     * */
    @DeleteMapping("/delete/{id}")
    public ResponseData<Boolean> delete(@PathVariable("id") int id){
        return new ResponseData<Boolean>(cartEntityService.removeById(id));
    }


}
