package com.study.shop.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Slf4j //日志注解，lombok依赖中的
@Controller // return返回的内容是页面
public class LoginContrlller {

    @GetMapping("login")
    public String login(){
        //跳转到login.html登录页
        return "login";
    }


}
