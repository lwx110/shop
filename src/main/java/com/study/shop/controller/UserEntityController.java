package com.study.shop.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.study.shop.base.ResponseData;
import com.study.shop.entity.UserEntity;
import com.study.shop.qo.UserEntityQo;
import com.study.shop.service.UserEntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserEntityController{

    @Resource
    private UserEntityService userEntityService;

    /**
     * 查询用户
     * */
    @GetMapping("/list")
    public ResponseData<List<UserEntity>> list(UserEntityQo qo){
        return new ResponseData<List<UserEntity>>(userEntityService.list(qo));
    }

    /**
     * 分页查询用户
     * */
    @GetMapping("/pageList")
    public ResponseData<Page<UserEntity>> pageList(UserEntityQo qo){
        return new ResponseData<Page<UserEntity>>(userEntityService.pageList(qo));
    }

    /**
     * 添加用户
     * */
    @PostMapping("/add")
    public ResponseData<Boolean> add(@RequestBody UserEntity userEntity){
        return new ResponseData<Boolean>(userEntityService.save(userEntity));
    }

    /**
     * 修改用户
     * */
    @PutMapping("/update")
    public ResponseData<Boolean> update(@RequestBody UserEntity userEntity){
        return new ResponseData<Boolean>(userEntityService.updateById(userEntity));
    }

    /**
     * 删除用户
     * */
    @DeleteMapping("/delete/{id}")
    public ResponseData<Boolean> delete(@PathVariable("id") int id){
        return new ResponseData<Boolean>(userEntityService.removeById(id));
    }


}
