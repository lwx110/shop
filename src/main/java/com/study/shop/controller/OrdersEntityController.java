package com.study.shop.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.study.shop.base.ResponseData;
import com.study.shop.qo.OrdersEntityQo;
import com.study.shop.service.OrdersEntityService;
import com.study.shop.vo.OrdersEntityVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/order")
@Slf4j
public class OrdersEntityController {

    @Resource
    private OrdersEntityService ordersEntityService;

    /**
     * 分页查询订单
     * */
    @GetMapping("/pageList")
    public ResponseData<Page<OrdersEntityVo>> pageList(@RequestParam Long current, @RequestParam Long size){
        return new ResponseData<Page<OrdersEntityVo>>(ordersEntityService.pageList(current,size));
    }

    /**
     * 查询某个用户订单
     * */
    @GetMapping("/detail/{userId}")
    public ResponseData<OrdersEntityVo> pageList(@PathVariable("userId") int userId){
        return new ResponseData<OrdersEntityVo>(ordersEntityService.detail(userId));
    }

    /**
     * 添加订单
     * */
    @PostMapping("/add")
    public ResponseData<Boolean> add(@RequestBody OrdersEntityQo ordersEntityQo){
        return new ResponseData<Boolean>(ordersEntityService.saveOrder(ordersEntityQo));
    }

    /**
     * 修改订单
     * */
    @PutMapping("/update")
    public ResponseData<Boolean> update(@RequestBody OrdersEntityQo ordersEntityQo){
        return new ResponseData<Boolean>(ordersEntityService.updataOrder(ordersEntityQo));
    }

    /**
     * 删除订单
     * */
    @DeleteMapping("/delete/{id}")
    public ResponseData<Boolean> delete(@PathVariable("id") int id){
        return new ResponseData<Boolean>(ordersEntityService.removeOrderId(id));
    }


}
