package com.study.shop.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController // return返回的内容是内容本身
public class IndexController {

    @GetMapping("/")
    public String getHello(){
        return "欢迎来到首页面";
    }

    @PostMapping ("/save")
    public String save(Map obj){
        log.info(obj.get("tel").toString());
        Map res = new HashMap();
        res.put("code",0);
        res.put("data",obj);
        return "0";
    }

}
