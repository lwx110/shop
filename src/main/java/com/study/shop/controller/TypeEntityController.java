package com.study.shop.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.study.shop.base.ResponseData;
import com.study.shop.entity.TypeEntity;
import com.study.shop.service.TypeEntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/type")
@Slf4j
public class TypeEntityController {

    @Resource
    private TypeEntityService typeEntityService;

    /**
     * 查询类型
     * */
    @GetMapping("/list")
    public ResponseData<List<TypeEntity>> list(@RequestParam String typeName){
        return new ResponseData<List<TypeEntity>>(typeEntityService.list(typeName));
    }

    /**
     * 分页查询类型
     * */
    @GetMapping("/pageList")
    public ResponseData<Page<TypeEntity>> pageList(@RequestParam String typeName,@RequestParam Long current,@RequestParam Long size){
        return new ResponseData<Page<TypeEntity>>(typeEntityService.pageList(typeName,current,size));
    }

    /**
     * 添加类型
     * */
    @PostMapping("/add")
    public ResponseData<Boolean> add(@RequestBody TypeEntity typeEntity){
        return new ResponseData<Boolean>(typeEntityService.save(typeEntity));
    }

    /**
     * 修改类型
     * */
    @PutMapping("/update")
    public ResponseData<Boolean> update(@RequestBody TypeEntity typeEntity){
        return new ResponseData<Boolean>(typeEntityService.updateById(typeEntity));
    }

    /**
     * 删除类型
     * */
    @DeleteMapping("/delete/{id}")
    public ResponseData<Boolean> delete(@PathVariable("id") int id){
        return new ResponseData<Boolean>(typeEntityService.removeById(id));
    }


}
