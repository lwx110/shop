package com.study.shop.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.study.shop.entity.TypeEntity;

import java.util.List;

public interface TypeEntityService extends IService<TypeEntity> {

    List<TypeEntity> list(String typeName);

    Page<TypeEntity> pageList(String typeName,Long current,Long size);

}
