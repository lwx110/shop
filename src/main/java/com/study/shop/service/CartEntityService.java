package com.study.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.study.shop.entity.CartEntity;
import com.study.shop.vo.CartEntityVo;

import java.util.List;

public interface CartEntityService extends IService<CartEntity> {

    List<CartEntityVo> listAll();

    CartEntityVo detail(int userId);

}
