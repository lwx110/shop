package com.study.shop.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.study.shop.entity.UserEntity;
import com.study.shop.qo.UserEntityQo;

import java.util.List;

public interface UserEntityService extends IService<UserEntity> {

    List<UserEntity> list(UserEntityQo qo);

    Page<UserEntity> pageList(UserEntityQo qo);

}
