package com.study.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.study.shop.entity.UserEntity;
import com.study.shop.mapper.UserEntityMapper;
import com.study.shop.qo.UserEntityQo;
import com.study.shop.service.UserEntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserEntityServiceImpl extends ServiceImpl<UserEntityMapper, UserEntity> implements UserEntityService {


    @Override
    public List<UserEntity> list(UserEntityQo qo) {
        return this.baseMapper.selectList(queryWrapper(qo));
    }

    @Override
    public Page<UserEntity> pageList(UserEntityQo qo) {
        Page<UserEntity> page = new Page<>();
        if (null == qo.getCurrent()){
            qo.setCurrent(1L);
        }
        if (null == qo.getSize()){
            qo.setSize(10L);
        }
        page.setCurrent(qo.getCurrent());
        page.setSize(qo.getSize());
        page = this.baseMapper.selectPage(page,queryWrapper(qo));
        return page;
    }

    public QueryWrapper queryWrapper(UserEntityQo qo){
        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
        if (null != qo && null != qo.getUserName()){
            queryWrapper.lambda().like(UserEntity::getUserName,qo.getUserName());
        }
        if (null != qo && null != qo.getRoleId()){
            queryWrapper.lambda().like(UserEntity::getRoleId,qo.getRoleId());
        }
        return queryWrapper;
    }


}
