package com.study.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.study.shop.entity.OrderDetailEntity;
import com.study.shop.entity.ProductEntity;
import com.study.shop.mapper.OrderDetailEntityMapper;
import com.study.shop.service.OrderDetailEntityService;
import com.study.shop.service.ProductEntityService;
import com.study.shop.vo.OrderDetailEntityVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class OrderDetailEntityServiceImpl extends ServiceImpl<OrderDetailEntityMapper, OrderDetailEntity> implements OrderDetailEntityService {


    @Resource
    private ProductEntityService productEntityService;

    @Override
    public List<OrderDetailEntityVo> detail(int orderId) {
        List<OrderDetailEntity> orderDetailEntityList = this.list(new QueryWrapper<OrderDetailEntity>().lambda().eq(OrderDetailEntity::getOrderId,orderId));
        List<OrderDetailEntityVo> orderDetailEntityVoList = new ArrayList<>();
        orderDetailEntityList.forEach(orderDetailEntity -> {
            orderDetailEntityVoList.add(getOrdersDetailEntityVo(orderDetailEntity));
        });
        return orderDetailEntityVoList;
    }

    public OrderDetailEntityVo getOrdersDetailEntityVo(OrderDetailEntity orderDetailEntity){
        OrderDetailEntityVo orderDetailEntityVo = new OrderDetailEntityVo();
        BeanUtils.copyProperties(orderDetailEntity, orderDetailEntityVo);
        ProductEntity productEntity = productEntityService.getById(orderDetailEntity.getProductId());
        if (null != productEntity){
            orderDetailEntityVo.setProductName(productEntity.getProductName());
        }
        return orderDetailEntityVo;
    }

}
