package com.study.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.study.shop.entity.ProductEntity;
import com.study.shop.entity.TypeEntity;
import com.study.shop.mapper.ProductEntityMapper;
import com.study.shop.qo.ProductEntityQo;
import com.study.shop.service.ProductEntityService;
import com.study.shop.service.TypeEntityService;
import com.study.shop.vo.ProductEntityVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ProductEntityServiceImpl extends ServiceImpl<ProductEntityMapper, ProductEntity> implements ProductEntityService {

    @Resource
    private TypeEntityService typeEntityService;

    @Override
    public List<ProductEntityVo> list(ProductEntityQo qo) {
        List<ProductEntity> list = this.baseMapper.selectList(queryWrapper(qo));
        List<ProductEntityVo> result = new ArrayList<>();
        list.stream().forEach(productEntity -> {
            result.add(getProductEntityVo(productEntity));
        });
        return result;
    }

    @Override
    public Page<ProductEntityVo> pageList(ProductEntityQo qo) {
        Page<ProductEntity> page = new Page<>();
        if (null == qo.getCurrent()){
            qo.setCurrent(1L);
        }
        if (null == qo.getSize()){
            qo.setSize(10L);
        }
        page.setCurrent(qo.getCurrent());
        page.setSize(qo.getSize());
        page = this.baseMapper.selectPage(page,queryWrapper(qo));
        List<ProductEntityVo> list = new ArrayList<>();
        page.getRecords().stream().forEach(productEntity -> {
            list.add(getProductEntityVo(productEntity));
        });
        Page<ProductEntityVo> result = new Page<>();
        result.setCurrent(page.getCurrent());
        result.setSize(page.getSize());
        result.setTotal(page.getTotal());
        result.setPages(page.getPages());
        result.setRecords(list);
        return result;
    }

    @Override
    public ProductEntityVo detail(int id) {
        ProductEntity productEntity = this.getById(id);
        return getProductEntityVo(productEntity);
    }

    public QueryWrapper queryWrapper(ProductEntityQo qo){
        QueryWrapper<ProductEntity> queryWrapper = new QueryWrapper<>();
        if (null != qo && null != qo.getProductName()){
            queryWrapper.lambda().like(ProductEntity::getProductName,qo.getProductName());
        }
        if (null != qo && null != qo.getTypeId()){
            queryWrapper.lambda().like(ProductEntity::getTypeId,qo.getTypeId());
        }
        return queryWrapper;
    }

    public ProductEntityVo getProductEntityVo(ProductEntity productEntity){
        ProductEntityVo productEntityVo = new ProductEntityVo();
        BeanUtils.copyProperties(productEntity,productEntityVo);
        TypeEntity typeEntity = typeEntityService.getById(productEntity.getTypeId());
        if (null != typeEntity){
            productEntityVo.setTypeName(typeEntity.getTypeName());
        }
        return productEntityVo;
    }


}
