package com.study.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.study.shop.entity.CartEntity;
import com.study.shop.entity.ProductEntity;
import com.study.shop.mapper.CartEntityMapper;
import com.study.shop.service.CartEntityService;
import com.study.shop.service.ProductEntityService;
import com.study.shop.vo.CartEntityVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CartEntityServiceImpl extends ServiceImpl<CartEntityMapper, CartEntity> implements CartEntityService {

    @Resource
    private ProductEntityService productEntityService;

    @Override
    public List<CartEntityVo> listAll() {
        List<CartEntity> list = this.list();
        List<CartEntityVo> result = new ArrayList<>();
        list.stream().forEach(cartEntity -> {
            result.add(getCartEntityVo(cartEntity));
        });
        return result;
    }


    @Override
    public CartEntityVo detail(int userId) {
        CartEntity cartEntity = this.getOne(new QueryWrapper<CartEntity>().lambda().eq(CartEntity::getUserId,userId));
        return getCartEntityVo(cartEntity);
    }

    public CartEntityVo getCartEntityVo(CartEntity cartEntity){
        CartEntityVo cartEntityVo = new CartEntityVo();
        BeanUtils.copyProperties(cartEntity,cartEntityVo);
        ProductEntity productEntity = productEntityService.getById(cartEntity.getProductId());
        if (null != productEntity){
            cartEntityVo.setProductName(productEntity.getProductName());
        }
        return cartEntityVo;
    }


}
