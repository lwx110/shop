package com.study.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.study.shop.entity.OrderDetailEntity;
import com.study.shop.entity.OrdersEntity;
import com.study.shop.mapper.OrdersEntityMapper;
import com.study.shop.qo.OrdersEntityQo;
import com.study.shop.service.OrderDetailEntityService;
import com.study.shop.service.OrdersEntityService;
import com.study.shop.vo.OrderDetailEntityVo;
import com.study.shop.vo.OrdersEntityVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrdersEntityServiceImpl extends ServiceImpl<OrdersEntityMapper, OrdersEntity> implements OrdersEntityService {

    @Resource
    private OrderDetailEntityService orderDetailEntityService;

    @Override
    public Page<OrdersEntityVo> pageList(Long current,Long size) {
        Page<OrdersEntity> page = new Page<>();
        if (null == current){
            page.setCurrent(1L);
        }
        if (null == size){
            page.setSize(10L);
        }
        page = this.baseMapper.selectPage(page,new QueryWrapper<OrdersEntity>());
        List<OrdersEntityVo> list = new ArrayList<>();
        page.getRecords().stream().forEach(ordersEntity -> {
            OrdersEntityVo ordersEntityVo = new OrdersEntityVo();
            BeanUtils.copyProperties(ordersEntity,ordersEntityVo);
            List<OrderDetailEntityVo> orderDetailEntityVoList = orderDetailEntityService.detail(ordersEntity.getId());
            ordersEntityVo.setOrderDetailEntityVoList(orderDetailEntityVoList);
            list.add(ordersEntityVo);
        });
        Page<OrdersEntityVo> result = new Page<>();
        result.setCurrent(page.getCurrent());
        result.setSize(page.getSize());
        result.setTotal(page.getTotal());
        result.setPages(page.getPages());
        result.setRecords(list);
        return result;
    }

    @Override
    public OrdersEntityVo detail(int userId) {
        List<OrdersEntity> ordersEntityList = this.list(new QueryWrapper<OrdersEntity>().lambda().eq(OrdersEntity::getUserId,userId)
                .orderByDesc(OrdersEntity::getCreateTime));
        OrdersEntityVo ordersEntityVo = new OrdersEntityVo();
        if (null != ordersEntityList && ordersEntityList.size() > 0){
            BeanUtils.copyProperties(ordersEntityList.get(0),ordersEntityVo);
            List<OrderDetailEntityVo> orderDetailEntityVoList = orderDetailEntityService.detail(ordersEntityList.get(0).getId());
            ordersEntityVo.setOrderDetailEntityVoList(orderDetailEntityVoList);
        }
        return null == ordersEntityVo ? null : ordersEntityVo;
    }

    @Override
    @Transactional
    public boolean saveOrder(OrdersEntityQo ordersEntityQo) {
        OrdersEntity ordersEntity = OrdersEntity.builder()
                .priceSum(ordersEntityQo.getPriceSum())
                .phone(ordersEntityQo.getPhone())
                .userId(ordersEntityQo.getUserId())
                .address(ordersEntityQo.getAddress()).build();
        this.save(ordersEntity);
        List<OrderDetailEntity> orderDetailEntityList = new ArrayList<>();
        ordersEntityQo.getOrderDetailEntityQoList().stream().forEach(orderDetailEntityQo -> {
            OrderDetailEntity orderDetailEntity = OrderDetailEntity.builder()
                    .orderId(ordersEntity.getId())
                    .num(orderDetailEntityQo.getNum())
                    .productId(orderDetailEntityQo.getProductId())
                    .price(orderDetailEntityQo.getPrice()).build();
            orderDetailEntityList.add(orderDetailEntity);
        });
        if (null != orderDetailEntityList && orderDetailEntityList.size() > 0){
            orderDetailEntityService.saveBatch(orderDetailEntityList);
        }
        return true;
    }

    @Override
    public boolean updataOrder(OrdersEntityQo ordersEntityQo) {
        return false;
    }

    @Override
    @Transactional
    public boolean removeOrderId(int id) {
        this.removeById(id);
        List<OrderDetailEntity> orderDetailEntityList = orderDetailEntityService.list(new QueryWrapper<OrderDetailEntity>()
                .lambda().eq(OrderDetailEntity::getOrderId,id));
        if (null != orderDetailEntityList && orderDetailEntityList.size() > 0){
            List<Integer> ids = orderDetailEntityList.stream().map(orderDetailEntity -> orderDetailEntity.getId()).collect(Collectors.toList());
            orderDetailEntityService.removeBatchByIds(ids);
        }
        return false;
    }


}
