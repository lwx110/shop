package com.study.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.study.shop.entity.TypeEntity;
import com.study.shop.entity.UserEntity;
import com.study.shop.mapper.TypeEntityMapper;
import com.study.shop.mapper.UserEntityMapper;
import com.study.shop.qo.UserEntityQo;
import com.study.shop.service.TypeEntityService;
import com.study.shop.service.UserEntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class TypeEntityServiceImpl extends ServiceImpl<TypeEntityMapper, TypeEntity> implements TypeEntityService {


    @Override
    public List<TypeEntity> list(String typeName) {
        return this.baseMapper.selectList(queryWrapper(typeName));
    }

    @Override
    public Page<TypeEntity> pageList(String typeName,Long current,Long size) {
        Page<TypeEntity> page = new Page<>();
        if (null == current){
            page.setCurrent(1L);
        }
        if (null == size){
            page.setSize(10L);
        }
        return this.baseMapper.selectPage(page,queryWrapper(typeName));
    }

    public QueryWrapper queryWrapper(String typeName){
        QueryWrapper<TypeEntity> queryWrapper = new QueryWrapper<>();
        if (null != typeName){
            queryWrapper.lambda().like(TypeEntity::getTypeName,typeName);
        }
        return queryWrapper;
    }


}
