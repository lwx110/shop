package com.study.shop.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.study.shop.entity.ProductEntity;
import com.study.shop.qo.ProductEntityQo;
import com.study.shop.vo.ProductEntityVo;

import java.util.List;

public interface ProductEntityService extends IService<ProductEntity> {

    List<ProductEntityVo> list(ProductEntityQo qo);

    Page<ProductEntityVo> pageList(ProductEntityQo qo);

    ProductEntityVo detail(int id);

}
