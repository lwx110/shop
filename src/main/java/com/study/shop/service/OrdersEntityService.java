package com.study.shop.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.study.shop.entity.OrdersEntity;
import com.study.shop.qo.OrdersEntityQo;
import com.study.shop.vo.OrdersEntityVo;


public interface OrdersEntityService extends IService<OrdersEntity> {


    Page<OrdersEntityVo> pageList(Long current, Long size);

    OrdersEntityVo detail(int userId);

    boolean saveOrder(OrdersEntityQo ordersEntityQo);

    boolean updataOrder(OrdersEntityQo ordersEntityQo);

    boolean removeOrderId(int id);

}
