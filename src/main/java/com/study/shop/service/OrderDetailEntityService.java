package com.study.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.study.shop.entity.OrderDetailEntity;
import com.study.shop.vo.OrderDetailEntityVo;

import java.util.List;


public interface OrderDetailEntityService extends IService<OrderDetailEntity> {

    List<OrderDetailEntityVo> detail(int orderId);

}
