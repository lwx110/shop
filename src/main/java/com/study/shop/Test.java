package com.study.shop;

import com.study.shop.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args){
        List<UserEntity> list = new ArrayList<>();

        list.add(UserEntity.builder().userName("lwx").password("123").build());
        list.add(UserEntity.builder().userName("lwx1").password("456").build());
        list.add(UserEntity.builder().userName("lwx").password("789").build());
        Map<String,UserEntity> map = list.stream().collect(Collectors.toMap(UserEntity::getUserName, Function.identity(),(key1,key2) -> key1));
        // 提交文件
        System.out.println(map);
    }


}
