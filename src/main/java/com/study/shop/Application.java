package com.study.shop;

import com.study.shop.Interceptor.LoginInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@MapperScan("com.study.shop.mapper")
public class Application implements WebMvcConfigurer {

    @Autowired
    private LoginInterceptor loginInterceptor;

    public void addInterceptors(InterceptorRegistry registry) {
        // 注册登录拦截器
        registry.addInterceptor(loginInterceptor)
                // 对哪些资源起过滤作用
                .addPathPatterns("/**")
                // 对哪些资源起排除作用
                .excludePathPatterns("/loginUser","/login.html","/css/**","/js/**");

    }


    public static void main(String[] args){
                 SpringApplication.run(Application.class,args);
   }

}
