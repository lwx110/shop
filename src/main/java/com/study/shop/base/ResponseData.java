package com.study.shop.base;

import lombok.Data;

@Data
public class ResponseData<T> {

    private int code;

    private String msg;

    private T data;

    ResponseData(){

    }

    public ResponseData(T data){
        this.code = 200;
        this.msg = "成功";
        this.data = data;
    }

    public ResponseData (int code,String msg,T data){
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

}
