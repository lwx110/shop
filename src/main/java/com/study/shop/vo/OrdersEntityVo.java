package com.study.shop.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class OrdersEntityVo {

    private int id;

    private String phone;

    private String address;

    private int userId;

    private double priceSum;

    private Date createTime;

    List<OrderDetailEntityVo> orderDetailEntityVoList;

}
