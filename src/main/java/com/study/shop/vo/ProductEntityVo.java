package com.study.shop.vo;

import lombok.Data;

import java.util.Date;

@Data
public class ProductEntityVo {

    private int id;

    private String productName;

    private int typeId;

    private int userId;

    private double price;

    private int count;

    private Date createTime;

    private String typeName;

}
