package com.study.shop.vo;

import lombok.Data;

import java.util.Date;

@Data
public class CartEntityVo {

    private int id;

    private int userId;

    private int productId;

    private int count;

    private Date createTime;

    private String productName;

}
