package com.study.shop.vo;

import lombok.Data;


@Data
public class OrderDetailEntityVo {

    private int id;

    private int orderId;

    private int productId;

    private int num;

    private double price;

    private String productName;


}
