package com.study.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.study.shop.entity.TypeEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TypeEntityMapper extends BaseMapper<TypeEntity> {

}
