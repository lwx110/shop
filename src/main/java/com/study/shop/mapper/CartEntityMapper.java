package com.study.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.study.shop.entity.CartEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CartEntityMapper extends BaseMapper<CartEntity> {

}
