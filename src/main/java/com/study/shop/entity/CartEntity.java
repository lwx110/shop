package com.study.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@TableName("cart")
public class CartEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;

    @TableField("userId")
    private int userId;

    @TableField("productId")
    private int productId;

    @TableField("count")
    private int count;

    @TableField("createTime")
    private Date createTime;



}
