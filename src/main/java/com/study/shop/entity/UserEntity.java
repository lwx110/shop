package com.study.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@TableName("user")
public class UserEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;

    @TableField("userName")
    private String userName;

    @TableField("password")
    private String password;

    @TableField("roleId")
    private int roleId;



}
