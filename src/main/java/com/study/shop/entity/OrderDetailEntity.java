package com.study.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@TableName("order_detail")
public class OrderDetailEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;

    @TableField("orderId")
    private int orderId;

    @TableField("productId")
    private int productId;

    @TableField("num")
    private int num;

    @TableField("price")
    private double price;

    @TableField("createTime")
    private Date createTime;



}
