package com.study.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@TableName("type")
public class TypeEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;

    @TableField("typeName")
    private String typeName;

    @TableField("createTime")
    private Date createTime;

    @TableField("userId")
    private int userId;



}
