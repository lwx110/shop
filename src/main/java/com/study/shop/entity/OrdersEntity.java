package com.study.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@TableName("orders")
public class OrdersEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;

    @TableField("phone")
    private String phone;

    @TableField("address")
    private String address;

    @TableField("userId")
    private int userId;

    @TableField("priceSum")
    private double priceSum;

    @TableField("createTime")
    private Date createTime;



}
